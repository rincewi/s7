from django.shortcuts import render
from django.contrib.auth.models import User
from django.db import models
#from django.db import connection
from django.http import HttpResponse

#def hello_sql(self):
#    with connection.cursor() as cursor:
#        cursor.execute("select username FROM auth_user")
#        row = cursor.fetchone()
#        name = "Hello %s" % row

#    return HttpResponse(name)

def hello_sql(self):

    users = User.objects.filter(is_superuser=True)
    name = "Hello %s" % users[0]

    return HttpResponse(name)

# Create your views here.
